package com.example.tester;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    //variables
    String currentNumberString = "0";
    double previousNumber;
    String pendingOperation = null;
    boolean clearDisplayOnNextDigit = false;
    boolean currentNumberStringHasDecimal = false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textDisplay = findViewById(R.id.display);


        //BUTOOJNS, with their onclick functions
        allClear = findViewById(R.id.all_clear);
        backspace = findViewById(R.id.backspace);
        signChange = findViewById(R.id.sign_change);
        divide = findViewById(R.id.divide);
        number1 = findViewById(R.id.number1);
        number2 = findViewById(R.id.number2);
        number3 = findViewById(R.id.number3);
        multiply = findViewById(R.id.multiply);
        number4 = findViewById(R.id.number4);
        number5 = findViewById(R.id.number5);
        number6 = findViewById(R.id.number6);
        subtract = findViewById(R.id.subtract);
        number7 = findViewById(R.id.number7);
        number8 = findViewById(R.id.number8);
        number9 = findViewById(R.id.number9);
        add = findViewById(R.id.add);
        decimal = findViewById(R.id.decimal);
        number0 = findViewById(R.id.number0);
        equals = findViewById(R.id.equals);

        allClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("A/C button clicked");

                //clears all numbers in memory/all variables
                currentNumberString = "0";
                previousNumber = 0.0;
                pendingOperation = null;
                clearDisplayOnNextDigit = false;
                currentNumberStringHasDecimal = false;

                //display the current number
                textDisplay.setText(currentNumberString);

            }
        });

        signChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println ("change sign button has been clicked");

                if (currentNumberString.startsWith("-")){
                    currentNumberString = currentNumberString.replace( "-",  "");
                }else{
                    currentNumberString = "-" + currentNumberString;
                }



                if (currentNumberString.equals("-0")) {
                    currentNumberString = currentNumberString.replace( "-0",  "0");

                }




                textDisplay.setText(currentNumberString);

            }
        });

        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentNumberString.length() - 1 == 0) {
                    textDisplay.setText("0");
                    previousNumber = 0;
                    currentNumberString = "";

                    return;

                }
                currentNumberString = currentNumberString.substring(0, currentNumberString.length() - 1);




                textDisplay.setText(currentNumberString);

            }
        });



        equals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("equal button pressaded");

                if (pendingOperation != null) {

                    double currentNumber = Double.parseDouble(currentNumberString);

                    if (pendingOperation.equals("Add")) {
                        previousNumber += currentNumber;
                    } else if (pendingOperation.equals("Subtract")) {
                        previousNumber -= currentNumber;
                    } else if (pendingOperation.equals("Multiply")) {
                        previousNumber *= currentNumber;
                    } else if (pendingOperation.equals("Divide")) {
                        previousNumber /= currentNumber;
                    }

                    currentNumberString = Double.valueOf(previousNumber).toString();

                    textDisplay.setText(currentNumberString);

                    previousNumber = Double.parseDouble(currentNumberString);

                    pendingOperation = null;

                    clearDisplayOnNextDigit = true;
                }
            }
        });

        decimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("decimal is da mf PRESSED DOE");
                // decimal is only placed when there is no current decimal
                if (!currentNumberStringHasDecimal) {
                    // put this at the stat of each number’s code
                    if (clearDisplayOnNextDigit) {
                        currentNumberString = "0";
                        clearDisplayOnNextDigit = false;

                    }

                    //check if it is 0 if it is then it goes boom boom
                    final String enteredDigit = ".";

                    currentNumberString += enteredDigit;


                    //display the friekn number
                    textDisplay.setText(currentNumberString);
                    // set an indicator that current number string has a decimal
                    currentNumberStringHasDecimal = true;
                }
            }
        });




        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("ADDING BOIIIII");

                //put this in the addition button code
                if (pendingOperation != null) {

                    double currentNumber = Double.parseDouble(currentNumberString);

                    if (pendingOperation.equals("Add")) {
                        previousNumber += currentNumber;
                    }
                    else if (pendingOperation.equals("Subtract"))  {
                        previousNumber -= currentNumber;
                    }
                    else if (pendingOperation.equals("Multiply"))  {
                        previousNumber *= currentNumber;
                    }
                    else if (pendingOperation.equals("Divide"))  {
                        previousNumber /= currentNumber;
                    }

                    currentNumberString = Double.valueOf(previousNumber).toString();

                    textDisplay.setText(currentNumberString);
                }
                previousNumber = Double.parseDouble(currentNumberString);

                pendingOperation = "Add";

                clearDisplayOnNextDigit = true;


            }
        });

        subtract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("subtracting is hard");

                //put this in the addition button code
                if (pendingOperation != null) {

                    double currentNumber = Double.parseDouble(currentNumberString);

                    if (pendingOperation.equals("Add")) {
                        previousNumber += currentNumber;
                    }
                    else if (pendingOperation.equals("Subtract"))  {
                        previousNumber -= currentNumber;
                    }
                    else if (pendingOperation.equals("Multiply"))  {
                        previousNumber *= currentNumber;
                    }
                    else if (pendingOperation.equals("Divide"))  {
                        previousNumber /= currentNumber;
                    }

                    currentNumberString = Double.valueOf(previousNumber).toString();

                    textDisplay.setText(currentNumberString);
                }
                previousNumber = Double.parseDouble(currentNumberString);

                pendingOperation = "Subtract";

                clearDisplayOnNextDigit = true;
            }
        });

        multiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("multiplying is a skill everyone should know");

                //put this in the addition button code
                if (pendingOperation != null) {

                    double currentNumber = Double.parseDouble(currentNumberString);

                    if (pendingOperation.equals("Add")) {
                        previousNumber += currentNumber;
                    }
                    else if (pendingOperation.equals("Subtract"))  {
                        previousNumber -= currentNumber;
                    }
                    else if (pendingOperation.equals("Multiply"))  {
                        previousNumber *= currentNumber;
                    }
                    else if (pendingOperation.equals("Divide"))  {
                        previousNumber /= currentNumber;
                    }

                    currentNumberString = Double.valueOf(previousNumber).toString();

                    textDisplay.setText(currentNumberString);
                }
                previousNumber = Double.parseDouble(currentNumberString);

                pendingOperation = "Multiply";

                clearDisplayOnNextDigit = true;


            }
        });

        divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("dividing is like multiplying, but worse");

                //put this in the addition button code
                if (pendingOperation != null) {

                    double currentNumber = Double.parseDouble(currentNumberString);

                    if (pendingOperation.equals("Add")) {
                        previousNumber += currentNumber;
                    }
                    else if (pendingOperation.equals("Subtract"))  {
                        previousNumber -= currentNumber;
                    }
                    else if (pendingOperation.equals("Multiply"))  {
                        previousNumber *= currentNumber;
                    }
                    else if (pendingOperation.equals("Divide"))  {
                        previousNumber /= currentNumber;
                    }

                    currentNumberString = Double.valueOf(previousNumber).toString();

                    textDisplay.setText(currentNumberString);
                }
                previousNumber = Double.parseDouble(currentNumberString);

                pendingOperation = "Divide";

                clearDisplayOnNextDigit = true;
            }
        });

        //numberrrrrrsssss
        number1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("one is prees");
                // put this at the stat of each number’s code
                if (clearDisplayOnNextDigit) {
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;


                }


                //check if it is 0 if it is then it goes boom boom
                final String enteredDigit = "1";
                if (currentNumberString.equals("0")) {

                    currentNumberString = enteredDigit;

                }
                else { // if display isnt 0, put 1 on
                    currentNumberString += enteredDigit;

                }


                //display the friekn number
                textDisplay.setText(currentNumberString);

            }
        });

        number2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("two is gamer");
                // put this at the stat of each number’s code
                if (clearDisplayOnNextDigit) {
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //check if it is 0 if it is then it goes boom boom
                final String enteredDigit = "2";
                if (currentNumberString.equals("0")) {

                    currentNumberString = enteredDigit;

                }
                else { // if display isnt 0, put 1 on
                    currentNumberString += enteredDigit;

                }


                //display the friekn number
                textDisplay.setText(currentNumberString);

            }
        });

        number3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("3 has press");
                // put this at the stat of each number’s code
                if (clearDisplayOnNextDigit) {
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //check if it is 0 if it is then it goes boom boom
                final String enteredDigit = "3";
                if (currentNumberString.equals("0")) {

                    currentNumberString = enteredDigit;

                }
                else { // if display isnt 0, put 1 on
                    currentNumberString += enteredDigit;

                }


                //display the friekn number
                textDisplay.setText(currentNumberString);

            }
        });

        number4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("four is many epic");
                // put this at the stat of each number’s code
                if (clearDisplayOnNextDigit) {
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //check if it is 0 if it is then it goes boom boom
                final String enteredDigit = "4";
                if (currentNumberString.equals("0")) {

                    currentNumberString = enteredDigit;

                }
                else { // if display isnt 0, put 1 on
                    currentNumberString += enteredDigit;

                }


                //display the friekn number
                textDisplay.setText(currentNumberString);


            }
        });

        number5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("gamer child five");
                // put this at the stat of each number’s code
                if (clearDisplayOnNextDigit) {
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //check if it is 0 if it is then it goes boom boom
                final String enteredDigit = "5";
                if (currentNumberString.equals("0")) {

                    currentNumberString = enteredDigit;

                }
                else { // if display isnt 0, put 1 on
                    currentNumberString += enteredDigit;

                }


                //display the friekn number
                textDisplay.setText(currentNumberString);


            }
        });

        number6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("six is cool");
                // put this at the stat of each number’s code
                if (clearDisplayOnNextDigit) {
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //check if it is 0 if it is then it goes boom boom
                final String enteredDigit = "6";
                if (currentNumberString.equals("0")) {

                    currentNumberString = enteredDigit;

                }
                else { // if display isnt 0, put 1 on
                    currentNumberString += enteredDigit;

                }


                //display the friekn number
                textDisplay.setText(currentNumberString);


            }
        });

        number7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("EPIC GAMER IS 7");
                // put this at the stat of each number’s code
                if (clearDisplayOnNextDigit) {
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //check if it is 0 if it is then it goes boom boom
                final String enteredDigit = "7";
                if (currentNumberString.equals("0")) {

                    currentNumberString = enteredDigit;

                }
                else { // if display isnt 0, put 1 on
                    currentNumberString += enteredDigit;

                }


                //display the friekn number
                textDisplay.setText(currentNumberString);

            }
        });

        number8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("eight is poopoohead");
                // put this at the stat of each number’s code
                if (clearDisplayOnNextDigit) {
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //check if it is 0 if it is then it goes boom boom
                final String enteredDigit = "8";
                if (currentNumberString.equals("0")) {

                    currentNumberString = enteredDigit;

                }
                else { // if display isnt 0, put 1 on
                    currentNumberString += enteredDigit;

                }


                //display the friekn number
                textDisplay.setText(currentNumberString);

            }
        });

        number9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("nine is the biggest number");
                // put this at the stat of each number’s code
                if (clearDisplayOnNextDigit) {
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //check if it is 0 if it is then it goes boom boom
                final String enteredDigit = "9";
                if (currentNumberString.equals("0")) {

                    currentNumberString = enteredDigit;

                }
                else { // if display isnt 0, put 1 on
                    currentNumberString += enteredDigit;

                }


                //display the friekn number
                textDisplay.setText(currentNumberString);


            }
        });

        number0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("zero clicked");
                // put this at the stat of each number’s code
                if (clearDisplayOnNextDigit) {
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //actual number stuff

                final String enteredDigit = "0";
                if (currentNumberString.equals("0")) {

                    currentNumberString = enteredDigit;

                }
                else { // if display isnt 0, put 1 on
                    currentNumberString += enteredDigit;

                }


                //display the friekn number
                textDisplay.setText(currentNumberString);


            }
        });




    }




    TextView textDisplay;

    Button allClear;
    Button backspace;
    Button signChange;
    Button divide;
    Button number1;
    Button number2;
    Button number3;
    Button multiply;
    Button number4;
    Button number5;
    Button number6;
    Button subtract;
    Button number7;
    Button number8;
    Button number9;
    Button add;
    Button decimal;
    Button number0;
    Button equals;





}






